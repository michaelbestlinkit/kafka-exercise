package bootcamp_kafka_exercise;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerRecord;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Properties;
import java.util.stream.Stream;

import static java.util.concurrent.TimeUnit.SECONDS;
import static bootcamp_kafka_exercise.FixedBatchSpliterator.withBatchSize;

public class ApplicationEntryPoint {
    static KafkaProducer<String, String> producer;

    static {
        Properties props = new Properties();
        props.put("bootstrap.servers", "10.15.19.138:9092");
        props.put("key.serializer", "org.apache.kafka.common.serialization.StringSerializer");
        props.put("value.serializer", "org.apache.kafka.common.serialization.StringSerializer");

        producer = new KafkaProducer<>(props);
    }
    public static void main(String[] args) throws IOException {
        String filename = args[0];
        Path folder = Paths.get("/opt/data");
        final Path inputPath = folder.resolve(filename);
        measureProcessing(withBatchSize(Files.lines(inputPath), 10));
        producer.close();
    }

    private static void measureProcessing(Stream<String> input) throws IOException {
        final long start = System.nanoTime();
        try (Stream<String> lines = input) {
            final long totalTime = lines.parallel()
                    .mapToLong(ApplicationEntryPoint::processLine).sum();
            final double cpuTime = totalTime, realTime = System.nanoTime() - start;
            final int cores = Runtime.getRuntime().availableProcessors();
            System.out.println("          Cores: " + cores);
            System.out.format("       CPU time: %.2f s\n", cpuTime / SECONDS.toNanos(1));
            System.out.format("      Real time: %.2f s\n", realTime / SECONDS.toNanos(1));
            System.out.format("CPU utilization: %.2f%%\n\n", 100.0 * cpuTime / realTime / cores);
        }
    }

    private static long processLine(String line) {
        final long localStart = System.nanoTime();
        producer.send(new ProducerRecord<>("zips", line));
        return System.nanoTime() - localStart;
    }
}

